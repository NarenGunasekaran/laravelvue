<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,100) as $index) {
            DB::table('users')->insert([
                'name' => $faker->name,
                'age' => rand(18,50),
                'salary' => rand(10000,50000),
                'city' => $faker->city
            ]);
        }
    }
}
