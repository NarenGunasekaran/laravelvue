/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
window.Vue.use(VueRouter);

import UsersIndex from './components/users/UsersIndex.vue';
import App from './components/App.vue';
import GraphData from './components/users/GraphData.vue';
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
// import VueCharts from 'vue-chartjs'
// import { Bar } from 'vue-chartjs'

const routes = [
    {
        path: '/',
        components: {
            appNew: App
        }
    },
    {
        path: '/getUsers',
        components: {
            usersIndex: UsersIndex
        }
    },
    {
        path: '/showGraph',
        components: {
            graph: GraphData
        }
    }
]

const router = new VueRouter({ routes })
const app = new Vue({ router }).$mount('#main-view')
