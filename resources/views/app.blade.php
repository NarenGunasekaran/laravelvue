<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <style>

        </style>
    </head>
    <body>
        <div id="main-view">
          <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="panel-body table-responsive">
                  <router-view name="appNew"></router-view>
                  <router-view name="usersIndex"></router-view>
                  <router-view name="graph"></router-view>
                </div>
            </div>
          </nav>
        </div>

        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
